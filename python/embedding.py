from sentence_transformers import SentenceTransformer

def embeddings_func(sentences):
    model = SentenceTransformer('sentence-transformers/all-MiniLM-L6-v2')
    embeddings = model.encode(sentences)
    print(embeddings)
    return embeddings