from flask import Flask, jsonify, request
from embedding import embeddings_func
from image_to_text import image_to_text_func

app = Flask(__name__)

# Route to get embedding sentence
@app.route('/embedding/sentence', methods=['GET'])
def get_sentence_embedding():
    sentence = request.args.get('sentence', '')
    if not sentence:
        return jsonify({"message": "No sentence provided"}), 400
    
    embeddings = embeddings_func(sentence)
    return jsonify({"embedding": embeddings.tolist()}), 200
    
# Route to get text from image
@app.route('/generate/text', methods=['GET'])
def get_text():
    imgPath = request.args.get('imgPath', '')
    if not imgPath:
        return jsonify({"message": "No image path provided"}), 400
    
    text = image_to_text_func(imgPath)
    return jsonify({"text": text}), 200

if __name__ == '__main__':
    app.run(debug=True)