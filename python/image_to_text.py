import requests
from PIL import Image
from transformers import BlipProcessor, BlipForConditionalGeneration

def image_to_text_func(imgPath):
    processor = BlipProcessor.from_pretrained("Salesforce/blip-image-captioning-large")
    model = BlipForConditionalGeneration.from_pretrained("Salesforce/blip-image-captioning-large")

    # Open the local image file
    raw_image = Image.open(imgPath).convert('RGB')

    # conditional image captioning
    text = "a picture of"
    inputs = processor(raw_image, text, return_tensors="pt")

    out = model.generate(**inputs)
    print(processor.decode(out[0], skip_special_tokens=True))
    return processor.decode(out[0], skip_special_tokens=True)