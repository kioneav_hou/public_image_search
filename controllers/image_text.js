require('dotenv').config();
const axios = require('axios');
const path = require('path')
const fs = require('fs');

async function testImageToText(req, res) {
    try {
        const imagePath = '/resource/images/1.jpg';
        const pathFile = path.join(__dirname, '..', imagePath);

        const response = await axios.get(
            `${process.env.BASE_URL_EMBEDDING}/generate/text?imgPath=${pathFile}`,
            {
                headers: {
                    'Content-Type': 'application/json',
                },
            }
        );
        if (response.status == 200) {
            return res.status(200).json({
                error: false,
                success: true,
                data: response.data.embedding
            });
        }
        return res.status(400).json({
            error: true,
            success: false,
        });
    } catch (error) {
        console.error('Error querying the model:', error);
        return res.status(400).json({
            error: true,
            success: false,
        });
    }
}
module.exports = testImageToText