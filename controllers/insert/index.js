
const { utilsResponseError } = require("../../utils/helper");
const fs = require('fs');
const axios = require('axios');
const { insertVector } = require("../../services/insert");
const path = require('path');
const { imageToText } = require("../../utils/image_to_text");
const { queriesVector } = require("../../services/queries");

const filePath = path.resolve(__dirname, '../../data/respond.json');

async function insertData (imageUrl, name, item_id) {
    const parsedUrl = new URL(imageUrl);
    const pathArray = parsedUrl.pathname.split('/');
    const filename = pathArray[pathArray.length - 1];
    const parts = filename.split('.');
    const extension = parts.length > 1 ? parts[parts.length - 1] : 'jpg';
    
    const imageName = `${item_id}.${extension}`;

    try {
        const download = await downloadImageAndFile(imageUrl, imageName);
        const queriesVectories = await queriesVector(item_id);
        if(queriesVectories.success && queriesVectories.data.length <= 0){
          const response = await imageToText('resource/images/'+imageName);
  
          if(response.success){
            const resultVector = await insertVector(response.data, name, item_id, imageUrl);
    
            if(download.success && resultVector.success){
                return {
                    error: false,
                    success: true,
                    data: resultVector.data
                };
            }
          }
        }
        return utilsResponseError();
    } catch (error) {
      return utilsResponseError();
    }

}

async function downloadImageAndFile(url, filename) {
    try {
      const response = await axios.get(url, {
        responseType: 'arraybuffer' // Ensure response is treated as binary data
      });
      
      // Check if the response status is 403 Forbidden
      if (response.status === 403 || response.status === 404) {
          console.log("Image download failed with status 403 Forbidden");
          return utilsResponseError();
      }
    
      if(response.data){
        await fs.writeFileSync('resource/images/'+filename, Buffer.from(response.data, 'binary'));
        return {
            error: false,
            success: true
        };
      }
      return utilsResponseError();
    } catch (error) {
      console.log("error", error);
      return utilsResponseError();
    }
}

async function readFileJsonDelishopToSave (req, res) {
    try {
      const jsonData = await fs.readFileSync(filePath, 'utf-8');
      const jsonObject = JSON.parse(jsonData);
      console.log("jsonData", jsonObject?.items.length);
  
      if(jsonObject?.items.length > 0 ){
        for(item of jsonObject.items) {
          if(item?.default_image_url != null && item?.default_image_url.trim() != ''){
            await insertData(item.default_image_url, item.name, item.item_id);
          }
        }
        return res.status(200).json({
            error: false,
            success: true,
        });
      }
      return res.status(400).json({
          error: true,
          success: false,
      });
    } catch (error) {
      console.error('Error reading JSON file:', error.message);
      return res.status(400).json({
          error: true,
          success: false,
      });
    }
}

module.exports = {
    readFileJsonDelishopToSave
}