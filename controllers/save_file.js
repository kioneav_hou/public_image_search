const fs = require('fs');
const axios = require('axios');
const path = require('path');
const { saveNewClass, checkClass } = require('../services/save_file');

async function downloadImageAndFile(req, res) {
    try {
        const data = req.body;
        const {filename} = req.query;

        for(const item of data){
            if(item?.title){
                const existClass = await checkClass(item?.title);
                if(existClass.success && existClass.found) {
                    return res.status(203).json({
                        error: true,
                        success: false,
                        data: {
                            message: "Item has been exist! Please select one"
                        }
                    });
                }else{
                    const newClass = await saveNewClass(item?.title);
                    if(newClass.success) {
                        item.brand_id = newClass.data.id
                    }
                }
            }
        }
        
        const textContent = data?.map(item => `${item.brand_id} ${item.width}, ${item.height}, ${item.x}, ${item.y}`).join('\n');

        // Path to the directory where the file will be saved
        const directoryPath = './resource/label'; // Replace this with the actual path

        // File path
        const filePath = `${directoryPath}/${filename}.txt`;

        // Save the text content to the file
        fs.writeFile(filePath, textContent, (err) => {
            if (err) {
                console.error('Error saving file:', err);
                return res.status(400).json({
                    error: true,
                    success: false,
                });
            }
            console.log('File saved successfully!');
        });
        return res.status(200).json({
            error: false,
            success: true,
            data: {
                message: "Successfully!"
            }
        });
    } catch (error) {
        console.log("error", error);
        return res.status(400).json({
            error: true,
            success: false,
        });
    }
}

async function getBrandObjects(req, res) {
    try {
        
        const jsonFileName = './data/brand.json';
        const jsonData = await fs.readFileSync(jsonFileName, 'utf-8');

        // Split the data into lines
        const lines = jsonData.split('\n');

        const jsonObject = []
        lines.forEach(line => {
            const index = line.indexOf(':');
            if (index !== -1) {
                const key = line.substring(0, index).trim();
                const value = line.substring(index + 1);
                jsonObject.push({
                    id: key,
                    brand_name: value
                })
            }
        });

        return res.status(200).json({
            error: false,
            success: true,
            data: jsonObject
        });
    } catch (error) {
        console.log("error", error);
        return res.status(400).json({
            error: true,
            success: false,
        });
    }
}

async function getExistObjectBrands (req, res) {
    try {
        const directoryPath = './resource/label/';
        const fileNames = getAllFileNames(directoryPath);

        const jsonFileName = './data/brand-object.json';
        const jsonData = await fs.readFileSync(jsonFileName, 'utf-8');
        const jsonObject = JSON.parse(jsonData);
        const objectNotExist = jsonObject.filter(item => !fileNames.includes(`${item.item_id}`))

        return res.status(200).json({
            error: false,
            success: true,
            data: objectNotExist
        });
    } catch (error) {
        console.log("error", error);
        return res.status(400).json({
            error: true,
            success: false,
        });
    }
}

// Function to get all file names in a directory
function getAllFileNames(directoryPath) {
    try {
        // Read the contents of the directory
        const files = fs.readdirSync(directoryPath);
        return files.map(fileName => fileName.replace(/\.[^.]+$/, ''));
    } catch (err) {
        console.error('Error reading directory:', err);
        return [];
    }
}

async function getFile (req, res) {
    try {
        const directoryPath = './resource/images/';
        const originalName = req.body.filename;

        getAllFilesInfoByOriginalName(originalName, directoryPath, (err, filesInfo) => {
            if (err) {
                console.error("Error:", err.message);
                return res.status(400).json({
                    error: true,
                    success: false,
                    message: err.message
                });
            } else {
                console.log(`Found files with original name '${originalName}'`);
                return res.status(200).json({
                    error: false,
                    success: true,
                    data: filesInfo
                });
            }
        });

    } catch (error) {
        console.log("error", error);
        return res.status(400).json({
            error: true,
            success: false,
        });
    }
}

async function getAllFilesInfoByOriginalName(originalName, directoryPath, callback) {
    fs.readdir(directoryPath, async (err, files) => {
        if (err) {
            callback(err);
            return;
        }

        const filesInfo = [];

        // Iterate through all files in the directory
        for (const file of files) {
            if (file.startsWith(originalName)) {
                const filePath = path.join(directoryPath, file);
                const stats = fs.statSync(filePath);
                const fileInfo = {
                    name: file,
                    path: filePath,
                    size: stats.size,
                    created_at: stats.birthtime,
                    modified_at: stats.mtime
                };

                // Get file type and Base64 data
                const fileBase64 = await getFileBase64(filePath);
                if (fileBase64) {
                    fileInfo.data = fileBase64.base64;
                    fileInfo.type = fileBase64.fileType;
                } else {
                    console.error(`Error reading file '${filePath}'.`);
                }

                filesInfo.push(fileInfo);
            }
        }

        if (filesInfo.length > 0) {
            callback(null, filesInfo);
        } else {
            callback(new Error(`No files found with the specified original name '${originalName}'`));
        }
    });
}

async function getFileBase64(filePath) {
    try {
        // Read file asynchronously
        const fileContent = await fs.promises.readFile(filePath);

        // Convert file content to Base64
        const base64Content = fileContent.toString('base64');

        // Determine file extension
        const fileExtension = path.extname(filePath).toLowerCase().substr(1);

        return {
            base64: `data:image/${fileExtension};base64,${base64Content}`,
            fileType: getFileType(fileExtension)
        };
    } catch (error) {
        return false
    }
}

function getFileType(fileExtension) {
    const imageExtensions = ['jpg', 'jpeg', 'png', 'gif', 'bmp'];
    if (imageExtensions.includes(fileExtension)) {
        return 'image';
    } else {
        return 'text';
    }
}

module.exports = {
    downloadImageAndFile,
    getBrandObjects,
    getExistObjectBrands,
    getFile
}