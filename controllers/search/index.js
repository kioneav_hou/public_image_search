const { searchData, deleteFileSearch } = require('../../services/search');
const { imageToText } = require('../../utils/image_to_text');
const { performOCR } = require('../../utils/ocr');

async function getResultSearch (req, res) {
    try {
        if(req.file) {
            const { originalname } = req.file;
            const response = await imageToText(`./uploads/${originalname}`);
            if(response.success){
                const combineResult = response.data;
                const search = await searchData(combineResult)
                if(search.success){
                    await deleteFileSearch(originalname)
                    return res.status(200).json({
                        error: false,
                        success: true,
                        data: search.data
                    });
                }
            }
            await deleteFileSearch(originalname)
        }
        return res.status(400).json({
            error: true,
            success: false,
        });
    } catch (error) {
        console.error('Error downloading image:', error);
        if(req.file) {
            const { originalname } = req.file;
            await deleteFileSearch(originalname)
        }
        return res.status(400).json({
            error: true,
            success: false,
        });
    }
}

module.exports = {
    getResultSearch
}