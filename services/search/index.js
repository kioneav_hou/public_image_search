require('dotenv').config();
const fs = require('fs');
const { MilvusClient } = require("@zilliz/milvus2-sdk-node");
const { Embedding } = require('../../utils/embedding');
const { utilsResponseError } = require('../../utils/helper');

const ZILLIZ_ADDRESS = process.env.ZILLIZ_ADDRESS;
const ZILLIZ_TOKEN = process.env.ZILLIZ_TOKEN;

async function searchVector (vector) {
    try {
        // Connect to the cluster
        const client = new MilvusClient({address: ZILLIZ_ADDRESS, token: ZILLIZ_TOKEN});
        const result = await client.search({
            collection_name: process.env.ZILLIZ_COLLECTION,
            vector: vector,
            topk: 20,
            // limit: 10,
            output_fields: ['image_url', 'name']
        })
    
        console.log("result", result);
        // const data = result.results.map(item => item.product_id);
        const resultData = result.results.reduce((acc, value) => {
            acc.push(
                {
                    image: value.image_url,
                    name: value.name
                }
            )
            return acc;
        }, [])
        return {
            error: false,
            success: true,
            data: resultData
        };
        
    }  catch (error) {
        console.log("error", error);
        return utilsResponseError();
    }
}

async function searchData (text) {
    try {
        const vector = await Embedding(text);
        if(vector.success){
            const result = await searchVector(vector.data);
            if(result.success){
                return {
                    error: false,
                    success: true,
                    data: result.data
                };
            }
        }
        return utilsResponseError();
        
    } catch (error) {
        console.log("error", error);
        return utilsResponseError();
    }
}

async function deleteFileSearch (filename) {
    const filePath = `uploads/${filename}`;

    // Check if the file exists
    fs.access(filePath, fs.constants.F_OK, (err) => {
        if (err) {
            return{
                error: true,
                success: false,
                data: {
                    message: 'File not found'
                }
            };
        }

        // Delete the file
        fs.unlink(filePath, (err) => {
            if (err) {
                return{
                    error: true,
                    success: false,
                    data: {
                        message: 'Error deleting file'
                    }
                };
            }
            return{
                error: false,
                success: true,
                data: {
                    message: 'File deleted successfully'
                }
            };
        });
    });
}

module.exports = {
    searchData,
    deleteFileSearch
}