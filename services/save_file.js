const fs = require('fs');
async function saveNewClass (name_class) {
    console.log("name_class", name_class);
    try {
        const jsonFileName = './data/brand.json';
        const jsonData = await fs.readFileSync(jsonFileName, 'utf-8');
        const numberRegex = /\d+:/g;

        // Extract numbers from the content
        const numbers = jsonData.match(numberRegex);
        const integers = numbers.map(item => {
            const number = item.split(":")[0];
            return parseInt(number.trim());
        });

        console.log("integers", integers.length);
        // return false
        if (integers.length > 0) {
            // Initialize max with the first element of the array
            let max = Math.max(...integers) + 1;

            // Append the new line to the file
            fs.appendFile('./data/brand.json', '\n' + `${max}: ${name_class}`, (err) => {
                if (err) {
                    console.error('Error appending to file:', err);
                    return {
                        error: true,
                        success: false,
                    };
                }
            });
            return {
                error: false,
                success: true,
                data: {
                    id: max
                }
            };
        }

    } catch (error) {
        console.error('Error downloading image:', error);
        return {
            error: true,
            success: false,
        };
    }
}

async function checkClass (name_class) {
    try {
        const jsonFileName = './data/brand.json';
        const data = await fs.promises.readFile(jsonFileName, 'utf-8');
        const lines = data.split('\n').map(line => line.trim());
        // Define the pattern to match
        const pattern = /:(.*)/;

        const values = lines.map(item => {
            const match = pattern.exec(item);
            return match && match[1] ? match[1].trim() : null;
        });
        
        if(values != null) {
            if(values.includes(name_class)){
                console.log("gg");
                return {
                    error: false,
                    success: true,
                    found: true
                };
            }
        }
        return {
            error: false,
            success: true,
            found: false
        };

    } catch (error) {
        console.error('Error downloading image:', error);
        return {
            error: true,
            success: false,
        };
    }
}

module.exports = {
    saveNewClass,
    checkClass
}