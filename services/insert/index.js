require('dotenv').config();
const axios = require('axios');
const { Embedding } = require('../../utils/embedding');
const { utilsResponseError } = require('../../utils/helper');

async function insertVector(text, name, item_id, image_url) {
    try {
        const vector = await Embedding(text);
        if (vector.success) {
            const options = {
                method: 'POST',
                url: 'https://in03-e2deeae3a6ca672.api.gcp-us-west1.zillizcloud.com/v1/vector/insert',
                headers: {
                    Authorization: 'Bearer e1d6d3452610e23915d6db2a2b50ddb66079a51b064b847eea45b8d37c0c6853b76befbce0009bcee46521f46553033848d738e9',
                    Accept: 'application/json',
                    'Content-Type': 'application/json'
                },
                data: {
                    collectionName: process.env.ZILLIZ_COLLECTION,
                    data: [
                        {
                            name: name,
                            product_id: item_id,
                            vector: vector.data,
                            image_url: image_url
                        }
                    ]
                }
            };
            const { data } = await axios.request(options);
            console.log("data", data);
            return {
                error: false,
                success: true,
                data: data
            };
        }
        return utilsResponseError();
    } catch (error) {
        console.error(error);
          return utilsResponseError();
    }
}

module.exports = {
    insertVector
}