require('dotenv').config();
const axios = require('axios');
const { utilsResponseError } = require('../../utils/helper');

async function queriesVector(product_id) {
    try {
        const options = {
            method: 'POST',
            url: 'https://in03-e2deeae3a6ca672.api.gcp-us-west1.zillizcloud.com/v1/vector/query',
            headers: {
                Authorization: 'Bearer e1d6d3452610e23915d6db2a2b50ddb66079a51b064b847eea45b8d37c0c6853b76befbce0009bcee46521f46553033848d738e9',
                Accept: 'application/json',
                'Content-Type': 'application/json'
            },
            data: {
                collectionName: process.env.ZILLIZ_COLLECTION,
                outputFields: ["product_id"],
                filter: `product_id in [${product_id}]`
            }
        };
        const { data } = await axios.request(options);
        console.log("data", data);
        if(data?.code == 200)
            return {
                error: false,
                success: true,
                data: data?.data
            };
        return utilsResponseError();
    } catch (error) {
        console.error(error);
        return utilsResponseError();
    }
}

module.exports = {
    queriesVector
}