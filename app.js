const express = require('express')
const cors = require('cors');
const bodyParser = require('body-parser');
const fs = require('fs');
const port = 3000
const app = express()

app.use(cors({ origin: '*' }));
app.use(bodyParser.json());
app.use(express.json());
app.use((req, res, next) => {
  res.setHeader('Access-Control-Allow-Origin', '*'); // Allow all origins
  res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS'); // Allow specific methods
  res.setHeader('Access-Control-Allow-Headers', 'Content-Type');
  next();
});

var saveFileRouter = require('./routes/save_file');
var searchImage = require('./routes');

// Define route for saving file
app.use('/api', saveFileRouter);
//search vector image
app.use('/api', searchImage);

// Serve static files from the 'public' directory
app.use(express.static('view'));
// Define route for serving the HTML file
app.get('/', (req, res) => {
    res.sendFile(__dirname + '/view/drawing.html');
});

app.listen(port, () => {
  console.log(`Example app listening on port ${port}`)
})