
let img;
let detector;
let objects = [];
let isDrawing = false;
let startX, startY, endX, endY;
const loader = document.getElementById('loader');
const section = document.getElementById('section');
section.style.display='none'

let brandsList = [];

function setup() {
    detector = ml5.objectDetector('cocossd', modelLoaded);
    let fileInput = createFileInput(handleFile);
    let container = select('#file-upload');
    let label = createElement('label', 'Choose your image');
    label.attribute('for', 'fileInput');
    label.addClass('form-label');
    fileInput.addClass('form-control position-static');
    fileInput.position(10, 10);
    label.parent(container)
    fileInput.parent(container);

    let canvas = createCanvas(400, 'auto');
    canvas.background(255);
    canvas.position(10, 50);
    canvas.mousePressed(startDrawing);
    canvas.mouseReleased(endDrawing);
}

function startDrawing() {
    let insideBoundingBox = checkingObjectAreas();
    // Check if the mouse is within the canvas boundaries
    let withinCanvas = mouseX >= 0 && mouseX <= width && mouseY >= 0 && mouseY <= height;
    let mouseOverLabel = isMouseOverLabel();
    let noBackGround = isPixelTransparent(mouseX, mouseY)

    // If mouse is not inside any bounding box and within the canvas boundaries, start drawing
    if (withinCanvas && !mouseOverLabel && !noBackGround) {
        isDrawing = true;
        startX = mouseX;
        startY = mouseY;
    }
}

function endDrawing() {
    let insideBoundingBox = checkingObjectAreas();
    // Check if the mouse is within the canvas boundaries
    let withinCanvas = mouseX >= 0 && mouseX <= width && mouseY >= 0 && mouseY <= height;
    let mouseOverLabel = isMouseOverLabel();
    let noBackGround = isPixelTransparent(mouseX, mouseY)

    if (withinCanvas && !mouseOverLabel && !noBackGround) {
        isDrawing = false;
        endX = mouseX;
        endY = mouseY;
        const countDefault = objects.filter(item => item.default === true);
        objects.push({
            default: true,
            x: Math.min(startX, endX),
            y: Math.min(startY, endY),
            width: Math.abs(endX - startX),
            height: Math.abs(endY - startY),
            label: countDefault.length <= 0 ? `Default` : `Default ${countDefault.length}`
        });
        startX = startY = endX = endY = undefined;
    }
    //load form again when end draw
    appendForms(objects);
    console.log("objects", objects);
}

function undoDrawing() {
    objects.pop(); // Remove the last drawn bounding box
    appendForms(objects);
}

function resetCanvas() {
    objects = []; // Clear the objects array
    appendForms(objects);
}


function handleFile(file) {
    console.log(file);
    if (file.type === 'image') {
        img = createImg(file.data, '', '', imageLoaded).hide();
    } else {
        alert('Please select an image file.');
    }
}

function imageLoaded() {
    detectObjects();
}

function modelLoaded() {
    console.log('Model loaded!');
    loader.classList.add("d-none");
    loader.style.display = 'none';
    section.classList.remove("d-none");
    section.classList.add("d-block");
}

function detectObjects() {
    detector.detect(img, gotDetections);
}

//detect object
function gotDetections(error, results) {
    if (error) {
        console.error(error);
        return;
    }
    objects = results;
    console.log(results)

    // Set canvas size to match image size
    const eleCanvas = createCanvas(img.width, img.height);
    eleCanvas.addClass('position-unset d-flex m-auto');
    // Set canvas size to match image size
    let container = select('#parent-img'); // Select the parent container
    if (container) {
        let childContainer = container.elt.querySelector('#container'); // Select the child element
        if (childContainer) {
            eleCanvas.parent(childContainer);
        } else {
            console.error("Child container not found.");
        }
    } else {
        console.error("Parent container not found.");
    }

    appendForms(objects);
}

//drawing
function draw() {
    if (img) {
        image(img, 0, 0);
        for (let i = 0; i < objects.length; i++) {
            let object = objects[i];
            noFill();
            stroke(255, 0, 0);
            strokeWeight(2);
            rect(object.x, object.y, object.width, object.height);
            noStroke();
            fill(255);
            textSize(16);
            text(object.label, object.x + 10, object.y + 20);
        }

        if (isDrawing) {
            noFill();
            stroke(0, 255, 0);
            strokeWeight(2);
            rect(startX, startY, mouseX - startX, mouseY - startY);
        }
    }
}

//select object want ot change
// function mouseClicked() {
//     let mouseOverLabel = isMouseOverLabel();
//     let clickedObjects = [];

//     if(mouseOverLabel) {
//         for (let i = 0; i < objects.length; i++) {
//             let object = objects[i];
//             if (mouseX >= object.x && mouseX <= object.x + object.width &&
//                 mouseY >= object.y && mouseY <= object.y + object.height) {
//                 clickedObjects.push(object);
//             }
//         }

//         if (clickedObjects.length > 0) {
//             let currentIndex = selectedObjectIndex;
//             let foundIndex = -1;

//             // Find the index of the next object with the same label
//             for (let i = 0; i < clickedObjects.length; i++) {
//                 let index = objects.findIndex(obj => obj === clickedObjects[i]);
//                 if (index > currentIndex) {
//                     foundIndex = index;
//                     break;
//                 }
//             }

//             // If no next object with the same label is found, start from the beginning
//             if (foundIndex === -1 && clickedObjects.length > 0) {
//                 foundIndex = objects.findIndex(obj => obj === clickedObjects[0]);
//             }

//             // Update the selected object index and display its label
//             selectedObjectIndex = foundIndex;
//             document.getElementById('inputObjectTitle').value = objects[selectedObjectIndex]?.label ?? '';
//         }
//     }
// }

function handelChangeObjectDetect(index) {
    let objectLabel = document.getElementById(`object_title_${index}`);
    objects[index].brandName = '';

    if (objectLabel && objectLabel.value) {
        objects[index].label = objectLabel.value;
    }
    console.log("objects", objects);
}

function handleChangeBrandsName(index) {
    let selectElement = document.getElementById(`selectBrandsName${index}`);
    let objectLabel = document.getElementById(`object_title_${index}`);
    objectLabel.value = '';
    objects[index].label = '';

    // Get the selected option
    var selectedOption = selectElement.options[selectElement.selectedIndex];

    if (selectElement && selectElement.value) {
        objects[index].brandName = selectedOption.textContent;
        objects[index].id = selectElement.value;
    }
}

function checkingObjectAreas() {
    // Check if the mouse is inside any existing bounding box
    let insideBoundingBox = false;
    for (let i = 0; i < objects.length; i++) {
        let object = objects[i];
        if (mouseX >= object.x && mouseX <= object.x + object.width &&
            mouseY >= object.y && mouseY <= object.y + object.height) {
            insideBoundingBox = true;
            break;
        }
    }
    return insideBoundingBox;
}

function isMouseOverLabel() {
    for (let i = 0; i < objects.length; i++) {
        let object = objects[i];
        let labelX = object.x; // Adjust these values based on your label positioning
        let labelY = object.y;
        let labelWidth = textWidth(object.label);
        let labelHeight = 30; // Assuming a fixed label height
        if (mouseX >= labelX && mouseX <= labelX + labelWidth &&
            mouseY >= labelY && mouseY <= labelY + labelHeight) {
            fill(200)
            // If mouse is over a label, return true
            return true;
        }
    }
    return false; // Mouse is not over any label
}

function isPixelTransparent(x, y) {
    // Get the color of the pixel at position (x, y)
    let pixelColor = get(x, y);
    // Check if the alpha value of the pixel color is 0 (transparent)
    return pixelColor[3] === 0;
}

function appendForms(objects) {

    let htmlForm = '';
    for (let i = 0; i < objects.length; i++) {
        htmlForm += "<tr id='object" + i + "'>";
        htmlForm += "<td>";
        htmlForm += "<input type=\"text\" name='object_title_" + i + "' placeholder='Enter Full Name' value='" + objects[i].label + "' id='object_title_" + i + "' class=\"form-control\" onkeyup=\"handelChangeObjectDetect(" + i + ")\" />"
        htmlForm += "</td>"
        htmlForm += "<td>"
        htmlForm += "<select id=\"selectBrandsName" + i + "\" class=\"form-select w-100\" onchange='handleChangeBrandsName(" + i + ")'>"
        htmlForm += "<option selected>Choose One</option>"
        for (let j = 0; j < brandsList.length; j++) {
            const element = brandsList[j];
            htmlForm += "<option value='"+element.id+"'>"+element.brand_name+"</option>"
        }
        htmlForm += "</select>"
        htmlForm += "</td>"
        htmlForm += "</tr>"
    }

    document.getElementById('body-content').innerHTML = htmlForm;

    for (let i = 0; i < objects.length; i++) {
        $('#selectBrandsName' + i).select2({
            theme: 'bootstrap-5'
        });
        $('#selectBrandsName' + i).next(".select2-container").css('width', '100%'); // Change the width as needed
    }
}

async function saveDataObjectsAsFileTxt() {
    const selectProductBrand = document.getElementById('selectProductBrand');

    let data = [];
    for (let i = 0; i < objects.length; i++) {
        data.push({
            width: objects[i]?.width.toFixed(2),
            height: objects[i]?.height.toFixed(2),
            x: objects[i]?.x.toFixed(2),
            y: objects[i]?.y.toFixed(2),
            brand_id: objects[i]?.id,
            title: objects[i]?.label,
            brand_name: objects[i]?.brandName
        })
    }

    fetch(`https://imagesearch.tmob.me/api/save/file?filename=${selectProductBrand.value}`, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(data)
    })
    .then(response => {
        if (!response.ok) {
            throw new Error('Network response was not ok');
        }
        return response.json();
    })
    .then(data => {
        console.log(data);
        if(data.success){
            // Clear forms and reset objects array
            clearForms();
            objects = [];
            redraw(); 
            getExistObjectsBransName()
            getObjectsBransName();
            // Display response in the HTML
            // document.getElementById('response-container').innerText = JSON.stringify(data);
        }else{
            if(data?.data?.message)
            var htmlAlert = '';
        
            htmlAlert += '<div class="row">'
                htmlAlert += '<div class="col-2"></div>'
                htmlAlert += '<div class="col-8">'
                    htmlAlert += '<div class="alert">'
                        htmlAlert += '<span class="closebtn" onclick="this.parentElement.style.display=\'none\';">&times;</span>'
                        htmlAlert += '<strong>Exist!</strong> '+data?.data?.message
                    htmlAlert += '</div>'
                htmlAlert += '</div>'
                htmlAlert += '<div class="col-2"></div>'
            htmlAlert += '</div>'
            $(htmlAlert).insertBefore('#select-object');
        }
    })
    .catch(error => {
        console.error('There was a problem with the fetch operation:', error);
    });
}

function clearForms() {
    // Clear forms by resetting the innerHTML of the container
    document.getElementById('body-content').innerHTML = '';
    img.remove();
    detectObjects();
    $('#selectProductBrand').val("Choose One").trigger('change');
}

async function handleChangeProductBrand () {
    const selectProductBrand = document.getElementById('selectProductBrand');
    if(selectProductBrand && selectProductBrand.value) {
        fetch('https://imagesearch.tmob.me/api/get/filename', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Accept': 'application/json'
            },
            body: JSON.stringify({filename: selectProductBrand.value})
        })
        .then(response => {
            if (!response.ok) {
                throw new Error('Network response was not ok');
            }
            return response.json();
        })
        .then(data => {
            console.log(data);
            if(data.success){
                if(data.data.length > 0)
                    handleFile(data.data[0])
            }
        })
        .catch(error => {
            console.error('There was a problem with the fetch operation:', error);
        });
    }
}

//call to get brands list
getObjectsBransName()
getExistObjectsBransName()

async function getObjectsBransName() {
    var html = '';
    fetch('https://imagesearch.tmob.me/api/get/brands', {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json'
        }
    })
    .then(response => {
        if (!response.ok) {
            throw new Error('Network response was not ok');
        }
        return response.json();
    })
    .then(data => {
        console.log(data);
        if(data.success){
            brandsList = data.data
        }
    })
    .catch(error => {
        console.error('There was a problem with the fetch operation:', error);
    });

}

async function getExistObjectsBransName() {
    var html = '';
    fetch('https://imagesearch.tmob.me/api/get/exist/brands', {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json'
        }
    })
    .then(response => {
        if (!response.ok) {
            throw new Error('Network response was not ok');
        }
        return response.json();
    })
    .then(data => {
        console.log(data);
        if(data.success){
            html += "<select id=\"selectProductBrand\" class=\"form-select w-100\" onchange='handleChangeProductBrand()'>"
            html += "<option selected>Choose One</option>"
            for (let i = 0; i < data.data.length; i++) {
                const element = data.data[i];
                html += "<option value='"+element.item_id+"'>"+element.brand_name+"</option>"
            }
            html += "</select>"
            document.getElementById('parent-selectProductBrand').innerHTML = html;

            //load select2
            $('#selectProductBrand').select2({
                theme: 'bootstrap-5'
            });
            $('#selectProductBrand').next(".select2-container").css('width', '100%'); // Change the width as needed
        }
    })
    .catch(error => {
        console.error('There was a problem with the fetch operation:', error);
    });

}
document.addEventListener('DOMContentLoaded', function() {

    const btnSave = document.getElementById('btnSave');
    if (btnSave) {
        btnSave.addEventListener('click', saveDataObjectsAsFileTxt);
    }
});