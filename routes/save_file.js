var express = require('express');
const { 
    downloadImageAndFile, 
    getBrandObjects ,
    getFile,
    getExistObjectBrands
} = require('../controllers/save_file');
const testImageToText = require('../controllers/image_text');
var router = express.Router();


router.post('/save/file', downloadImageAndFile);
router.post('/get/filename', getFile)
router.get('/get/brands', getBrandObjects)
router.get('/get/exist/brands', getExistObjectBrands)
router.get('/test/image/text', testImageToText)

module.exports = router;