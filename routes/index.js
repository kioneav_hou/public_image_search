var express = require('express');
const { getResultSearch } = require('../controllers/search');
const upload = require('../utils/multer');
const { readFileJsonDelishopToSave } = require('../controllers/insert');
var router = express.Router();

router.post('/search/image',  upload.single('image'), getResultSearch);
router.post('/insert/static/vector', readFileJsonDelishopToSave);

module.exports = router;