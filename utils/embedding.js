require('dotenv').config();
const OpenAI = require("openai");
// const axios = require('axios');
const { utilsResponseError } = require('./helper');

const openai = new OpenAI({
    apiKey: process.env.OPENAI_API_KEY
});

async function Embedding(text) {
  try {
    const embedding = await openai.embeddings.create({
      model: process.env.OPENAI_MODEL,
      input: text,
      encoding_format: "float",
    });
    
    console.log("embedding", embedding);
    if(embedding.data && embedding.data.length > 0){
      return {
        error: false,
        success: true,
        data: embedding.data[0].embedding
      };
    }
    return utilsResponseError();
  } catch (error) {
    console.log("error", error);
    return utilsResponseError();
  }
}

// async function Embedding(text) {
//   try {
//     const response = await axios.get(
//       `${process.env.BASE_URL_EMBEDDING}/embedding/sentence?sentence=${text}`,
//       {
//         headers: {
//           'Content-Type': 'application/json',
//         },
//       }
//     );
//     if(response.status == 200){
//       return {
//         error: false,
//         success: true,
//         data: response.data.embedding
//       };
//     }
//     return utilsResponseError();
//   } catch (error) {
//     console.error('Error querying the model:', error);
//     return utilsResponseError();
//   }
// }

module.exports = {
    Embedding
}