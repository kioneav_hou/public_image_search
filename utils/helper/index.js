function utilsResponseError () {
    return{
        error: true,
        success: false,
    };
}

module.exports = {
    utilsResponseError
}