const Tesseract = require('tesseract.js');
const { utilsResponseError } = require('./helper');

async function performOCR(imagePath) {
  try {
      const { data: { text } } = await Tesseract.recognize(
          imagePath,
          'eng', // language
          { 
            tessdata: './model-extraction/eng.traineddata', 
            tessedit_char_whitelist: 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789', // specify characters to recognize
          } // logger
      );
      return {
        error: false,
        success: true,
        data: text
      };
  } catch (error) {
      console.error('Error:', error);
      return utilsResponseError();
  }
}
module.exports = {
  performOCR
}