require('dotenv').config();
const fs = require('fs');
const axios = require('axios');
const path = require('path')
const { utilsResponseError } = require('./helper');

async function imageToText(filename) {
  const data = fs.readFileSync(filename);

  try {
    const response = await axios.post(
      process.env.HUGGING_FACE_MODEL_URL,
      data,
      {
        headers: {
          Authorization: `Bearer ${process.env.HUGGING_FACE_API_TOKEN}`,
          'Content-Type': 'image/jpeg',
        },
      }
    );
    console.log("bbb", response.data[0].generated_text);
    return {
      error: false,
      success: true,
      data: response.data[0].generated_text
    };
  } catch (error) {
    console.error('Error querying the model:', error);
    return utilsResponseError();
  }
}

// async function imageToText(filename) {
//   try {
//     const pathFile = path.join(__dirname, '..', filename);

//     const response = await axios.get(
//         `${process.env.BASE_URL_EMBEDDING}/generate/text?imgPath=${pathFile}`,
//         {
//             headers: {
//                 'Content-Type': 'application/json',
//             },
//         }
//     );
//     console.log("response", response);
//     if (response.status == 200) {
//         return {
//             error: false,
//             success: true,
//             data: response.data.embedding
//         };
//     }
//     return utilsResponseError();
//   } catch (error) {
//       console.error('Error querying the model:', error);
//       return utilsResponseError();
//   }
// }

module.exports = {
    imageToText
}